package com.devcamp.countryregion.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.countryregion.models.Country;

@Service
public class CountryService {
    @Autowired
    private RegionService regionService;

    private Country vietNam = new Country("VN", "Việt Nam");
    private Country trungQuoc = new Country("TQ", "Trung Quốc");
    private Country usa = new Country("USA", "Mỹ");

    public ArrayList<Country> getAllCountry() {
        ArrayList<Country> countryList = new ArrayList<>();

        vietNam.setRegions(regionService.getVietNamRegion());
        trungQuoc.setRegions(regionService.getTrungQuocRegion());
        usa.setRegions(regionService.getUSARegion());

        countryList.add(vietNam);
        countryList.add(trungQuoc);
        countryList.add(usa);

        return countryList;
    }

    public Country findCountry(String countryCode) {
        ArrayList<Country> countryList = new ArrayList<>();

        vietNam.setRegions(regionService.getVietNamRegion());
        trungQuoc.setRegions(regionService.getTrungQuocRegion());
        usa.setRegions(regionService.getUSARegion());

        countryList.add(vietNam);
        countryList.add(trungQuoc);
        countryList.add(usa);

        for (Country country : countryList) {
            if(country.getCountryCode().equals(countryCode)) {
                return country;
            }
        }

        return null;
    }
}
