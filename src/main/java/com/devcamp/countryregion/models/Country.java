package com.devcamp.countryregion.models;

import java.util.ArrayList;

public class Country {
    private String countryName;
    private String countryCode;
    private ArrayList<Region> regions;
    
    public Country() {
    }

    public Country(String countryCode, String countryName) {
        this.countryName = countryName;
        this.countryCode = countryCode;
    }

    public Country(String countryName, String countryCode, ArrayList<Region> regions) {
        this.countryName = countryName;
        this.countryCode = countryCode;
        this.regions = regions;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public ArrayList<Region> getRegions() {
        return regions;
    }

    public void setRegions(ArrayList<Region> regions) {
        this.regions = regions;
    }
}
