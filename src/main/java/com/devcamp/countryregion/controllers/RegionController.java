package com.devcamp.countryregion.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.countryregion.models.Region;
import com.devcamp.countryregion.services.RegionService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class RegionController {
    @Autowired
    private RegionService regionService;

    @GetMapping("/find-region")
    public Region findRegion(@RequestParam String regionCode) {
        return regionService.findRegion(regionCode);
    }
}
